.model small
.stack 100h
.data
Var1 db "Deerani $" 
Var2 db 0ah,0dh,"TwixteR $"  
.code

main proc  
        
 mov ax,@data   
 mov ds,ax
 
 mov dl,Offset Var1
 mov ah,09
 int 21h
  mov ax,@data
 mov ds,ax
 mov dl,Offset Var2
 mov ah,09
 int 21h       

main endp
end main